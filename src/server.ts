import net, { Server, Socket, AddressInfo } from "net";

interface IPromiseFiber<T = unknown> {
    promise: Promise<T>;
    resolve: (...args: T[]) => void;
    reject: (err: unknown) => void;
}

interface IOptions {
    /**
     * A custom logging function.
     */
    logger?: (message: unknown) => void;
    /**
     * Whether or not to enable debug logging.
     */
    logging?: boolean;
    /**
     * What host the proxy server should listen on. Normally you should set this to 0.0.0.0.
     */
    host: string;
    /**
     * What port the proxy server should listen on.
     */
    port: number;
    /**
     * This defines what the this server should forward to.
     */
    forward: {
        host: string;
        port: number;
    };
}

// ____________________ Class constructor
export class ProxyServer {
    public port?: number;
    public serverListeningPromiseFiber: IPromiseFiber<Server>;
    public server: Server;
    log: (message: unknown) => void;

    constructor(public readonly options: IOptions) {
        this.serverListeningPromiseFiber = {} as IPromiseFiber<Server>;
        this.serverListeningPromiseFiber.promise = new Promise(
            (resolve, reject) => {
                this.serverListeningPromiseFiber.resolve = resolve;
                this.serverListeningPromiseFiber.reject = reject;
            }
        );
        this.log = options.logger || console.log.bind(console);
        if (!this.options.logging) this.log = () => {};
    }

    public awaitStartedListening(): Promise<Server> {
        return this.serverListeningPromiseFiber.promise;
    }

    start() {
        try {
            const server = net.createServer();

            server.on("connection", this.onConnection.bind(this));

            server.on("error", (err) => {
                this.log("SERVER ERROR");
                this.log(err);
            });

            server.on("close", () => {
                this.log("Client Disconnected");
            });

            server.listen(this.options, () => {
                if (this.serverListeningPromiseFiber)
                    this.serverListeningPromiseFiber.resolve(server);
            });
            this.server = server;
            return this;
        } catch (err) {
            this.log("ERROR ON CREATE: ::::: CATCH");
            this.log(err);
            throw err;
        }
    }
    onConnection(clientTothisSocket: Socket) {
        this.log(`Client Connected To this: ${clientTothisSocket.address()}`);
        // We need only the data once, the starting packet
        clientTothisSocket.once("data", (data) => {
            this.log("Create this to server connection ...");
            const thisToServerSocket = net
                .createConnection(
                    {
                        host: this.options.forward.host,
                        port: this.options.forward.port || 80,
                    },
                    () => {
                        thisToServerSocket.write(data);

                        // Piping the sockets
                        clientTothisSocket.pipe(thisToServerSocket);
                        thisToServerSocket.pipe(clientTothisSocket);
                    }
                )
                .on("error", (err) => {
                    this.log("this TO SERVER ERROR");
                    this.log(err);
                });

            clientTothisSocket.on("error", (err) => {
                this.log("clientTothis socket error: :::::::");
                this.log(err);
            });
        });
    }
}
