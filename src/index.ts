import { readFile } from "@theoparis/config";
import * as z from "zod";
import { ProxyServer } from "./server";
import { onShutdown } from "node-graceful-shutdown";

const portRegex = /(\d{1,5})-(\d{1,5})*$/g;

export const configSchema = z.object({
    forward: z.array(
        z.object({
            from: z.string(),
            port: z
                .string()
                .refine((s) => s.match(portRegex))
                .transform(z.custom<{ from: number; to: number }>(), (s) => ({
                    from: parseInt(s.split("-")[0]),
                    to: parseInt(s.split("-")[1]),
                })),
        })
    ),
});

export type Config = z.infer<typeof configSchema>;

export const config = readFile<Config>(`${process.cwd()}/config.yaml`, {
    type: "yaml",
    schema: configSchema,
}).validate(true);

config.toObject().forward.forEach((f) => {
    for (let i = f.port.from; i < f.port.to; i++) {
        const server = new ProxyServer({
            logging: false,
            port: i,
            host: "0.0.0.0",
            forward: { host: f.from, port: i },
        }).start();
        server
            .awaitStartedListening()
            .then((s) => {
                onShutdown(async () => {
                    s.close();
                });
            })
            .catch(console.error);
    }
    console.log(`Forwarding ${f.from} TO 0.0.0.0:${f.port.from}-${f.port.to}`);
});
