# @flowtr/forward

A command-line package that lets you forward a range of ports from another machine.

## Usage

First, create a configuration file like this called `config.yaml`:

```yaml
forward:
    - from: "1234.1234.1234.1234" # The host to forward from
      port: "25500-25600" # The ports to forward from the host to the local machine
```

Then run the following in a terminal that is in the folder containg the config file:

```bash
fowd
```
